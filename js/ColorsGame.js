var app = angular.module("myApp", []);

app.controller("myCtrl", function($scope, $interval) {

  const config = {
    interval: '',
    time: 1500,
    addColor: 0,
    colors: [
      {name: "czarny", rgb: "rgb(0, 0, 0)"},
      {name: "brązowy", rgb : "rgb(204, 102, 0)"},
      {name: "niebieski", rgb : "rgb(0, 0, 255)"},
      {name: "zielony", rgb : "rgb(0, 1028, 0)"}
      ]
  };

  const Random = (elm) => {
    var shuffle, randNr;

    shuffle = function(){
      for(var j, x, i = elm.length; i; j = parseInt(Math.random() * i), x = elm[--i], elm[i] = elm[j], elm[j] = x);
      return elm;
    };

    randNr  = function(){
      return Math.floor((Math.random() * elm.length-1) + 1);
    };

    return {shuffle: shuffle,
            randNr: randNr}
  };

  const Level = () => {
    var moreColors, speed;
    $scope.points++;

    moreColors = () => {
      var colors = [
        {name: "złóty", rgb: "rgb(255, 255, 0)"},
        {name: "fioletowy", rgb: "rgb(153, 0, 153)"},
        {name: "czerwony", rgb: "rgb(255, 0, 0)" },
        {name: "różowy", rgb: "rgb(255, 51, 102)"}
      ];

      if(colors[config.addColor]){
        config.colors.push(colors[config.addColor]);
        config.addColor++;
      }
    };

    speed = () => {
      config.time -= 100;
    }

    return {
      moreColors: moreColors,
      speed: speed
    }
  }

  const Points = () => {
    var add, fail;

    add = () => {
      $scope.points++;
    };

    fail = () => {
      $scope.points--;
      $scope.fail++;
    }

    return {
      add: add,
      fail: fail}
  }

  const Display = () => {
    var init, view;
    init = () => {
      interval.start();
      $scope.points = 0;
      $scope.fail = 0;
    };

    view = () => {
      var colors, random;
      colors = config.colors;
      random = Random(colors);

      $scope.colors = random.shuffle();
      $scope.randName = random.shuffle()[random.randNr()].name;
      $scope.randColor = random.shuffle()[random.randNr()].rgb;
    }

    return {
    init: init,
    view: view}
  }
  var display = Display();

  const Interval = (elm) => {
    var start, stop, restart;

    start = () => {
      config.interval = $interval(elm, config.time);
    };

    stop = () => {
      $interval.cancel(config.interval);
    };

    restart = () => {
      stop();
      start();
    };

    return {
      start: start,
      stop: stop,
      restart: restart}
  };
  var interval = Interval(display.view);

  var End = () => {
      interval.stop();
      $( "#dialog" ).dialog( "open" );
  }

  $scope.play = (choice, check) => {
      display.view();
      interval.restart();

      if($scope.fail == 3){
        End();
      }

      if(choice == check){
        Points().add();

        for(let i = 20, x = 5; i <= 80, x <= 10000; i += 20, x += 5){

          if($scope.points == i){
            Level().moreColors();
          };

          if($scope.points == x){
            Level().speed();
          };

        }
    } else {
      Points().fail();
    }
  };

  display.init();
  display.view();
});
